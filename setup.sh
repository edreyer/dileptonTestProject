setupATLAS
lsetup root
asetup AnalysisBase,21.2.1,here

#Make directories if needed
mkdir -p build source run
cp -n CMakeLists.txt source

#Build the project
cd build
rm CMakeCache.txt
cmake ../source
make
cd ..